#################################################################################################
#
#  BMS TEXT CLASSIFICATION APPLICATION
#
#  Last modified: 02/2019
#
#  Programmer: Jose Joaquin Mesa Jimenez
#  
#  This script contains the functions that perform the calculations for the software and are called
#  from all other scripts.
#
###################################################################################################

import sys
sys.path.append("./Tagging/lib/python3.6/site-packages")

import numpy as np
import pandas as pd

import re

import matplotlib.pyplot as plt
from random import shuffle

#Locate and list all xlms files so they can be opened all together

import os
from sklearn.preprocessing import LabelBinarizer, MultiLabelBinarizer

from sklearn.model_selection import train_test_split

from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.multiclass import OneVsRestClassifier

from skmultilearn.problem_transform import LabelPowerset

from sklearn.svm import LinearSVC

from IPython.display import Markdown, display
def printmd(string):
    display(Markdown(string))

def read_files():
    xlms_files = []
    for root, dirs, files in os.walk("."):  
        for filename in files:
            if filename[-4:]=='xlsm':  #Add to the list if the element's last four spaces form the word "xlsm"
                xlms_files += [filename]
                
    return xlms_files
        
def output_for_skyspark(xlms_files):
    #concatenate all files into a huge dataset

    inputs = []
    
    for file in xlms_files:
        file = "Spreadsheets/" + file
        xl = pd.ExcelFile(file)
        if "Input" in xl.sheet_names:
            df = xl.parse('Output for SkySpark') #Go to "Tagging" tab
            inputs += [df] 
            
    inputs = pd.concat(inputs) #Concatenate them one below the next one
    inputs = inputs.dropna(subset=["service"]) #Drop row if service value is equals zero
    inputs = inputs.reset_index() #Restart the index from zero to its length
    inputs = inputs.drop(columns=["index"]) #Drop old index
    inputs = inputs.fillna(0)
    
    return inputs

def tagging_sheet(xlms_files):
    
    #concatenate all files into a huge dataset
    
    inputs = []
    
    for file in xlms_files:
        file = "Spreadsheets/" + file
        xl = pd.ExcelFile(file)
        if "Input" in xl.sheet_names:
            df = xl.parse('Tagging') #Go to "Tagging" tab
            df.columns = df.iloc[1] #Re-define index
            df = df.reindex(df.index.drop(0)) #Drop the other two columns
            df = df.reindex(df.index.drop(1))
            df = df.dropna(thresh=5) #Discard all the rows with at least 5 NaN elements
            inputs += [df] 
    
    inputs = pd.concat(inputs) #Concatenate them one below the next one
    inputs = inputs.dropna(subset=["Service"]) #Drop row if service value is equals zero
    inputs = inputs.reset_index() #Restart the index from zero to its length
    inputs = inputs.drop(columns=["index"]) #Drop old index
    inputs = inputs.fillna(0)
    
    return inputs

def load_validation_sheet():
    xlms_files = []
    for root, dirs, files in os.walk("/home/waki/validate"):  
        for filename in files:
            if filename[-4:]=='xlsm':  #Add to the list if the element's last four spaces form the word "xlsm"
                xlms_files += [filename]
    
    #concatenate all files into a huge dataset

    inputs = []
    
    for file in xlms_files:
        file = "/home/waki/validate/" + file
        xl = pd.ExcelFile(file)
        if "Input" in xl.sheet_names:
            df = xl.parse('Output for SkySpark') #Go to "Tagging" tab
            inputs += [df] 
            
    inputs = pd.concat(inputs) #Concatenate them one below the next one
    inputs = inputs.dropna(subset=["service"]) #Drop row if service value is equals zero
    inputs = inputs.reset_index() #Restart the index from zero to its length
    inputs = inputs.drop(columns=["index"]) #Drop old index
    inputs = inputs.fillna(0)
    
    return inputs

def load_validation_sheet_tagging():
    
    xlms_files = []
    for root, dirs, files in os.walk("/home/waki/validate"):  
        for filename in files:
            if filename[-4:]=='xlsm':  #Add to the list if the element's last four spaces form the word "xlsm"
                xlms_files += [filename]
    
    #concatenate all files into a huge dataset

    inputs = []

    for file in xlms_files:
        file = "/home/waki/validate/" + file
        xl = pd.ExcelFile(file)
        if "Input" in xl.sheet_names:
            df = xl.parse('Tagging') #Go to "Tagging" tab
            df.columns = df.iloc[1] #Re-define index
            df = df.reindex(df.index.drop(0)) #Drop the other two columns
            df = df.reindex(df.index.drop(1))
            df = df.dropna(thresh=5) #Discard all the rows with at least 5 NaN elements
            inputs += [df] 
            
    inputs = pd.concat(inputs) #Concatenate them one below the next one
    inputs = inputs.dropna(subset=["Service"]) #Drop row if service value is equals zero
    inputs = inputs.reset_index() #Restart the index from zero to its length
    inputs = inputs.drop(columns=["index"]) #Drop old index
    inputs = inputs.fillna(0)
    
    return inputs

class SequenceIterator:
    
    def __init__(self, dataset, label_points, ref_points, type_points, unit_points, outputs, dictionary):
        
        self.dataset = dataset
        self.label_points = label_points
        self.type_points = type_points
        self.unit_points = unit_points
        self.ref_points = ref_points
        self.outputs = outputs
        
        self.labels = dataset[label_points]
        self.refs = dataset[ref_points]
        self.types = dataset[type_points]
        self.units = dataset[unit_points]
        self.y = dataset[outputs]
        
        self.dictionary = dictionary
              
    def __iter__(self):
        for label,ref,one_type,unit,tags in zip(self.labels, self.refs, self.types, self.units, self.y):
            
            label = str(label) + " " + ref[0] + " " + str(one_type) + " " + str(unit)
            #Eliminate numbers?
            label = re.sub(r"[0-9]","",label)
            label = re.sub("[.]","",label)
            #Lower case characters
            label = label.lower().split()
            
            #tags = re.sub(","," ",tags)
            #tags = tags.split()
            tags = str(tags)
            #Clean it from dictionary            
            for i in range(0,len(label)):
                if label[i] in self.dictionary:
                    label[i] = self.dictionary[label[i]][0]
                    
            words = [w for w in label]
            
            yield words, tags
            
class SequenceTags:
    def __init__(self, dataset, label_points, ref_points, type_points, unit_points, outputs, dictionary):
        
        self.dataset = dataset
        self.label_points = label_points
        self.type_points = type_points
        self.unit_points = unit_points
        self.ref_points = ref_points
        self.outputs = outputs
        
        self.labels = dataset[label_points]
        self.refs = dataset[ref_points]
        self.types = dataset[type_points]
        self.units = dataset[unit_points]
        self.y = dataset[outputs]
        
        self.dictionary = dictionary
              
    def __iter__(self):
        for label,ref,one_type,unit,tags in zip(self.labels, self.refs, self.types, self.units, self.y):
            
            label = label + " " + ref[0] + " " + str(one_type) + " " + str(unit)
            #Eliminate numbers?
            label = re.sub(r"[0-9]","",label)
            label = re.sub("[.]","",label)
            #Lower case characters
            label = label.lower().split()
            
            tags = re.sub(","," ",tags)
            tags = tags.split()
            
            #Clean it from dictionary            
            for i in range(0,len(label)):
                if label[i] in self.dictionary:
                    label[i] = self.dictionary[label[i]][0]
                    
            words = [w for w in label]
            
            yield words, tags

#This function encodes the words into binary instances located at some position of a vector of size number_of_words
def encoder_onehot(y, multiclass = True):
    
    if multiclass == True:
        
        multilabel_binarizer = LabelBinarizer()
    else:
        multilabel_binarizer = MultiLabelBinarizer()
        
    multilabel_binarizer.fit(y)
    unique_tags_listed = multilabel_binarizer.classes_
    y_onehot = multilabel_binarizer.fit_transform(y)
    
    return y_onehot, unique_tags_listed


def data_prep(words, words_val, y_onehot, unique_tags_listed):
    
    data_val = pd.DataFrame([" ".join(i) for i in words_val],columns=["Label"])
    labels = [" ".join(label) for label in words]
    labels_df = pd.DataFrame(labels, columns=["Label"])
    tags_df = pd.DataFrame(y_onehot,columns=unique_tags_listed)
    categories = list(tags_df.columns.values)
    print(categories)
    data = pd.concat([labels_df,tags_df], axis=1)
    
    return data, data_val, categories

#Vecotoriser function. It takes input data (training and validation), the % used to split the data and the number of repetitions
def data_preprocessing(data, data_val, test_size, n_repetitions):
    
    train, test = train_test_split(data, random_state=42, test_size=test_size, shuffle=True)

    print(train.shape)
    print(test.shape)

    train_text = train['Label']
    test_text = test['Label']
    data_val = data_val["Label"]
    
    vectorizer = TfidfVectorizer(strip_accents='unicode', analyzer='word', ngram_range=(1,3), norm='l2')
    vectorizer.fit(train_text)
    #vectorizer.fit(test_text)
    
    data_val = vectorizer.transform(data_val)
    
    x_train = vectorizer.transform(train_text)
    y_train = train.drop(labels = ['Label'], axis=1)
    
    x_test = vectorizer.transform(test_text)
    y_test = test.drop(labels = ['Label'], axis=1)

    x_train = np.repeat(x_train.toarray(), n_repetitions, axis=0)
    y_train = pd.DataFrame(np.repeat(y_train.values,n_repetitions,axis=0),columns=list(y_train))
    
    return x_train, y_train, x_test, y_test, data_val

def service_type(x_train, y_train, x_test, y_test, data_val, unique_tags_listed, inputs, validate):
    
    # initialize label powerset multi-label classifier
    classifier = LabelPowerset(LogisticRegression())
    
    # train
    classifier.fit(x_train, y_train)
    
    # predict
    predictions = classifier.predict(x_test)
    predictions_val = classifier.predict(data_val)
    
    # accuracy
    print("Accuracy = ",accuracy_score(y_test,predictions))
    print("\n")
    
    results_table = predictions_val.toarray()
    #Get tags -> add to list -> filter empty spaces
    predicted_labels = [list(filter(None, (results_table[i]*unique_tags_listed.astype(object)).tolist())) for i in range(0,len(results_table))]

    #Join in a single string for presentation purposes
    predicted_labels = [", ".join(string) for string in predicted_labels]
    
    predicted_df = pd.DataFrame(predicted_labels,columns=["Predicted Service"])
    
    #index_list = y_test.index.tolist()
    actual_labels_and_tags = validate[["pointLabel","service"]]
    results = pd.concat([actual_labels_and_tags,predicted_df],axis=1)
    
    #Get the accuracy of the new spreadsheet prediction
    logic = (results["service"] == results["Predicted Service"]).tolist()
    total_accuracy = (logic.count(True)-logic.count(False))/len(logic)*100

    print("New dataset accuracy = ",total_accuracy)
    
    return results

def equipTags(x_train, y_train, x_test, y_test, data_val, unique_tags_listed, inputs, validate):
    
    # initialize label powerset multi-label classifier
    classifier = LabelPowerset(LogisticRegression())
    
    # train
    classifier.fit(x_train, y_train)
    
    # predict
    predictions = classifier.predict(x_test)
    predictions_val = classifier.predict(data_val)
    
    # accuracy
    print("Accuracy = ",accuracy_score(y_test,predictions))
    print("\n")
    
    results_table = predictions_val.toarray()
    #Get tags -> add to list -> filter empty spaces
    predicted_labels = [list(filter(None, (results_table[i]*unique_tags_listed.astype(object)).tolist())) for i in range(0,len(results_table))]

    #Join in a single string for presentation purposes
    predicted_labels = [", ".join(string) for string in predicted_labels]
    
    predicted_df = pd.DataFrame(predicted_labels,columns=["Predicted equipTags"])
    
    #index_list = y_test.index.tolist()
    actual_labels_and_tags = validate[["pointLabel","equipTags"]]
    results = pd.concat([actual_labels_and_tags,predicted_df],axis=1)
    
    #Get the accuracy of the new spreadsheet prediction
    logic = (results["equipTags"] == results["Predicted equipTags"]).tolist()
    total_accuracy = (logic.count(True)-logic.count(False))/len(logic)*100

    print("New dataset accuracy = ",total_accuracy)
    
    return results

def pointTags(x_train, y_train, x_test, y_test, data_val, unique_tags_listed, inputs, validate):
    
    # initialize label powerset multi-label classifier
    classifier = LabelPowerset(LinearSVC())
    
    # train
    classifier.fit(x_train, y_train)
    
    # predict
    predictions = classifier.predict(x_test)
    predictions_val = classifier.predict(data_val)
    
    # accuracy
    print("Accuracy = ",accuracy_score(y_test,predictions))
    print("\n")
    
    results_table = predictions_val.toarray()
    #Get tags -> add to list -> filter empty spaces
    predicted_labels = [list(filter(None, (results_table[i]*unique_tags_listed.astype(object)).tolist())) for i in range(0,len(results_table))]

    #Join in a single string for presentation purposes
    predicted_labels = [", ".join(string) for string in predicted_labels]
    
    predicted_df = pd.DataFrame(predicted_labels,columns=["Predicted pointTags"])
    
    #index_list = y_test.index.tolist()
    actual_labels_and_tags = validate[["pointLabel","pointTags"]]
    results = pd.concat([actual_labels_and_tags,predicted_df],axis=1)
    
    #Get the accuracy of the new spreadsheet prediction
    logic = (results["pointTags"] == results["Predicted pointTags"]).tolist()
    total_accuracy = (logic.count(True)-logic.count(False))/len(logic)*100

    print("New dataset accuracy = ",total_accuracy)
    
    return results

def augmentation(data, threshold):

    augmented_data = data.copy()
    
    categories = list(data.drop(labels = ['Label'], axis=1))
    
    for tag in categories:
        
        n_tags = data[tag].tolist().count(1)
        
        while n_tags < threshold:
            
            #Capture the elements of the tag in question
            points_to_shuffle = data[data[tag] == 1].reset_index(drop=True)
            #Isolate labels and split them into lists of words
            x = [points_to_shuffle["Label"].tolist()[i].split() for i in range(0, len(points_to_shuffle["Label"].tolist()))]
            #Shuffle the words amongst them for every label
            [shuffle(x[i]) for i in range(0, len(x))]
            #Join them again as complete sentences
            x_joined = [" ".join(i) for i in x]
            #Get back the training instances
            y = points_to_shuffle.drop(labels = ["Label"], axis = 1)
            #New labels transformed into dataframe 
            x_new = pd.DataFrame(x_joined, columns=["Label"])
            #New labels + old training instances
            to_add = pd.concat([x_new,y], axis = 1)
            #Add it to the augmented dataset
            augmented_data = pd.concat([augmented_data,to_add]).reset_index(drop = True)
            #Count again the number of tags to ensure it is not under the threshold
            n_tags = augmented_data[tag].tolist().count(1)
            
    return augmented_data

def augmentation_single(data, tag, threshold):

    augmented_data = data.copy()
    
    n_tags = data[tag].tolist().count(1)
            
    while n_tags < threshold:       
            
        #Capture the elements of the tag in question
        points_to_shuffle = data[data[tag] == 1].reset_index(drop=True)
        #Isolate labels and split them into lists of words
        x = [points_to_shuffle["Label"].tolist()[i].split() for i in range(0, len(points_to_shuffle["Label"].tolist()))]
        #Shuffle the words amongst them for every label
        [shuffle(x[i]) for i in range(0, len(x))]
        #Join them again as complete sentences
        x_joined = [" ".join(i) for i in x]
        #Get back the training instances
        y = points_to_shuffle.drop(labels = ["Label"], axis = 1)
        #New labels transformed into dataframe 
        x_new = pd.DataFrame(x_joined, columns=["Label"])
        #New labels + old training instances
        to_add = pd.concat([x_new,y], axis = 1)
        #Add it to the augmented dataset
        augmented_data = pd.concat([augmented_data,to_add]).reset_index(drop = True)
        #Count again the number of tags to ensure it is not under the threshold
        n_tags = augmented_data[tag].tolist().count(1)
            
    return augmented_data

# ACCURACY FUNCTIONS

def accuracy_per_tag(results_table, y_onehot_val, unique_tags_listed_val, save = False):
        
    accuracy_per_tag = [(len((results_table[:,i] == y_onehot_val[:,i]).tolist())-(results_table[:,i] == y_onehot_val[:,i]).tolist().count(False))/len((results_table[:,i] == y_onehot_val[:,i]).tolist())*100 for i in range(0,y_onehot_val.shape[1])]
    accuracy = pd.DataFrame(accuracy_per_tag, index=unique_tags_listed_val, columns=["Accuracy per tag type"])
    
    if save == True:
        
        accuracy.to_csv("Accuracy per tag type.csv")
        
    return accuracy

def results(y_onehot_val, unique_tags_listed_val, unique_tags_listed, results_table, validate, accuracy = False):
    
    actual_labels = [list(filter(None, (y_onehot_val[i]*unique_tags_listed_val.astype(object)).tolist())) for i in range(0,len(y_onehot_val))]
    actual_labels = [", ".join(string) for string in actual_labels]
    
    actual_df = pd.DataFrame(actual_labels,columns=["pointTags"])
    
    predicted_labels = [list(filter(None, (results_table[i]*unique_tags_listed.astype(object)).tolist())) for i in range(0,len(results_table))]
    
    #Join in a single string for presentation purposes
    predicted_labels = [", ".join(string) for string in predicted_labels]
    
    predicted_df = pd.DataFrame(predicted_labels,columns=["Predicted pointTags"])
    
    actual_labels_and_tags = validate[["navName"]]
    
    logic = (actual_df["pointTags"] == predicted_df["Predicted pointTags"]).tolist()
    logic_df = pd.DataFrame(logic,columns=["Success"])
    
    results = pd.concat([actual_labels_and_tags.reset_index(drop=True),actual_df,predicted_df,logic_df],axis=1)
    
    total_accuracy = (len(logic)-logic.count(False))/len(logic)*100
    
    if accuracy == True:
        
        return total_accuracy
    
    return results

def ROC_table(y_onehot_val, results_table, unique_tags_listed_val, save = False):
    
    rates = []
    
    for i in range(0, y_onehot_val.shape[1]):
        
        #Get the indexes of the positions where the true 0 and 1 are
        true_index = [i for i,x in enumerate(y_onehot_val[:,i]) if x == 1]
        false_index = [i for i,x in enumerate(y_onehot_val[:,i]) if x == 0]
        
        #Locate those index on the results table
        predicted_true = np.array([results_table[j,i] for j in true_index])
        predicted_false = np.array([results_table[j,i] for j in false_index])
        
        #Create the all zero/all ones vectors
        trues = np.ones(len(predicted_true)).astype(int)
        false = np.zeros(len(predicted_false)).astype(int)
        
        len_trues = len(trues)
        len_false = len(false)
        
        if len_trues == 0:
            len_trues = 1
            
        if len_false == 0:
            len_false = 1
        
        #Calculate True Possitive and True Negative values
        TPR = (predicted_true == trues).tolist().count(True)/len_trues*100
        TNR = (predicted_false == false).tolist().count(True)/len_false*100
        
        FNR = 100 - TPR
        FPR = 100 - TNR
        
        rates += [[TPR, TNR, FPR, FNR, len(trues)]]
        
    ROC_Table = pd.DataFrame(rates, columns=["True Positive Rate","True Negative Rate","False Positive Rate","False Negative Rate","Nº of Labels"], index=unique_tags_listed_val)
        
    if save == True:
        ROC_Table.to_csv("ROC_Table.csv")
        
    return ROC_Table

def relative_accuracy(y_onehot_val, results_table):
    TNRs = []

    for i in range(0, y_onehot_val.shape[0]):
        
        #Get the indexes of the positions where the true 0 and 1 are
        true_index = [i for i,x in enumerate(y_onehot_val[i,:]) if x == 1]
        false_index = [i for i,x in enumerate(y_onehot_val[i,:]) if x == 0]
        
        #Locate those index on the results table
        predicted_true = np.array([results_table[i,j] for j in true_index])
        predicted_false = np.array([results_table[i,j] for j in false_index])
        
        #Create the all zero/all ones vectors
        trues = np.ones(len(predicted_true)).astype(int)
        false = np.zeros(len(predicted_false)).astype(int)
        
        #Calculate True Possitive and True Negative values
        TPR = (predicted_true == trues).tolist().count(True)/len(trues)*100
        TNR = (predicted_false == false).tolist().count(True)/len(false)*100
        
        FNR = 100 - TPR
        FPR = 100 - TNR
        
        TNRs += [TNR]
        
        total_accuracy = np.array(TNRs).astype(int).tolist().count(100)/len(TNRs)
        
    return total_accuracy

def color_check_red(val):
    """
    Takes a scalar and returns a string with
    the css property `'color: red'` for negative
    strings, black otherwise.
    """
    
    #if type(val)==float or type(val)==int:
    color = 'red' if val < 0.5 else 'black'
    if val == 0:
        color = 'black' 
    return 'color: %s' % color

def table_proba(results_proba, results_table, unique_tags_listed, validate):
    
    results_possitive = results_proba*results_table
    actual_labels_and_tags = validate[["navName"]]
    actual_labels_and_tags.to_csv("Outputs.csv")
    
    tags_proba_df = pd.DataFrame(results_possitive, columns = unique_tags_listed)
    actual_labels_and_tags = actual_labels_and_tags.style
    tags_proba_colored_df = tags_proba_df.style.applymap(color_check_red)
    
    writer = pd.ExcelWriter('output.xlsx')
    tags_proba_colored_df.to_excel(writer,'Sheet1')
    writer.save()





