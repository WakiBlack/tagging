import pandas as pd
import numpy as np
from datetime import datetime

import urllib
# noinspection PyUnresolvedReferences
from lxml import objectify
from collections import OrderedDict

#from urllib2 import Request
import xml.etree.ElementTree as ET

#from Parameters import url as refer, windsolarreport, demandreport

#Definition of the custom functions made for the API data extraction

def BMRS_GetXML(**kwargs):
    # this function returns an XML object with our data for the given settlementdate & report keyword args
    url = "https://api.bmreports.com/BMRS/{report}/v1?APIKey=pu73x92zcobzacp&ServiceType=xml".format(**kwargs)
    for key, value in kwargs.items():
        if key not in ['report']: # report is removed because it is not a keyword arg, appears earlier in the URL
            a = "&%s=%s" % (key, value)
            url = url + a
    print(url)
    xml = objectify.parse(urllib.request.urlopen(url))
    return xml
 
def BMRS_Dataframe(**kwargs):
    # this function processes the XML object into a dataframe
    # navigates through to find the item in responseList
    # finds the root tag and builds an ordered dictionary, used to find the children tags
    result = None
    numerror = 0
    while result is None:
        try:
            tags = []
            output = []
            for root in BMRS_GetXML(**kwargs).findall("./responseBody/responseList/item/"):
                tags.append(root.tag)
                
            tag = OrderedDict((x, 1) for x in tags).keys()
            df = pd.DataFrame(columns=tag)
            
            for root in BMRS_GetXML(**kwargs).findall("./responseBody/responseList/item"):
                data = root.getchildren()
                output.append(data)
                
            df = pd.DataFrame(output, columns=tag)
            return df
        
        except Exception as exception:
            
            print(type(exception).__name__ )
            numerror = numerror + 1
            print(numerror)
            assert type(exception).__name__ == 'NameError'
            
def xml2df(data_xml):
    #This function is used specifically to extract NDF and TSDF data,
    #as written this way it performs better than the first one for this
    #specific case. The functioning is more simple, it looks through all
    #the items contained in responseBody/responseList and extracts all
    #the elements inside those items
    try:
        data=[]
        root=data_xml.getroot()
        for item in root.responseBody.responseList.item:
            el_data={}
            for child in item.getchildren():
                el_data[child.tag]=child.text
            data.append(el_data)

        df_data=pd.DataFrame(data)
    except:
        print("Error when extracting data from ELEXON. Check permissions or ELEXON circulars at www.elexon.com")
    
    return df_data

def get_weekdays(days_list):
    
    DateStamp_new=np.zeros(len(days_list)).tolist()
    weekday=np.zeros(len(days_list)).tolist()
    
    for i in range(len(days_list)):
        DateStamp_new[i]=datetime.strptime(str(days_list[i]), "%Y-%m-%d")#Convert to Datetime
        #DateStamp_new[i]=datetime.strftime(DateStamp_new[i], "%d-%m-%Y")#Converted into string format
        #DateStamp_new[i]=datetime.strptime(str(DateStamp_new[i]), "%d-%m-%Y")#Back to DateTime
        weekday[i]=DateStamp_new[i].weekday()
        DateStamp_new[i] = datetime.strftime(DateStamp_new[i],  "%Y-%m-%d")
    
    hour_format=pd.date_range(start="00:00",periods=len(days_list), freq="30T").strftime("%H:%M").tolist()

    #for i in range(len(hour_format)):
    #    hour_format[i]=hour_format[i].encode("ascii","ignore")#.decode("utf-8", "ignore") #Eliminates de "u" transforming from unicode
        
    DateStamp_new=pd.DataFrame((DateStamp_new),columns=["DateStamp"])
    weekday=pd.DataFrame(weekday,columns=["Weekday"])
    hour=pd.DataFrame(hour_format,columns=["Hour"])
    
    try:
        assert(len(weekday)==len(DateStamp_new)==len(hour)) #!! This conditions MUST be fulfilled by the function
    except AssertionError:
        print("ERROR: Check assertion on line 98 in API_ELEXON.py")
    #which means that the strings must have the same length
    return weekday, DateStamp_new, hour

def Today_ELEXON(now):
    # This is not used now, Period_Elexon is used for both user cases
    #The tags which are going to be added to the url that will call the data
    #are defined here in dictionaries. The data required is contained in two
    #reports: B1440 and FORDAYDEM
    renewables_key={'report':'B1440','SettlementDate':now,'Period':'*','Data col name':'WindAndSolar'}
    
    input_D1=dict(renewables_key)
    del input_D1['Data col name']
    print(input_D1) 
    
    #collecting data using function that grabs XML
    data_ren = xml2df(BMRS_GetXML(**input_D1))

    dayahead_key={'report':'FORDAYDEM','FromDate':now,'ToDate':now,'Data col name':'DayAheadDemand'}
    
    input_D2=dict(dayahead_key)
    del input_D2['Data col name']
    print(input_D2)  
    
    #collecting data using function that grabs XML
    data_day = xml2df(BMRS_GetXML(**input_D2))
    
    data_ren = pd.DataFrame(data_ren)
    data_day = pd.DataFrame(data_day)
    data_ren = data_ren.loc[data_ren['processType'].str.lower() == 'day ahead']
    
    NDF=data_day[data_day.recordType=='DANF'][['settlementDate','settlementPeriod','demand']].rename(index=str, columns={"settlementDate":"SD","settlementPeriod": "SP","demand":"NDF"})
    TSDF=data_day[data_day.recordType=='DATF'][['settlementPeriod','demand']].rename(index=str, columns={"settlementPeriod": "SP","demand":"TSDF"}).set_index(["SP"])

    days_list_ren=NDF["SD"].values.tolist()
    
    weekdays_ren, DateStamp_ren, hour_ren = get_weekdays(days_list_ren)
    
    weekdays_ren.index=NDF["SP"].values.tolist()
    DateStamp_ren.index=NDF["SP"].values.tolist()
    hour_ren.index=NDF["SP"].values.tolist()
    
    NDF=NDF.set_index(["SP"])
    
    dayahead1=pd.concat([NDF["NDF"],TSDF],axis=1)
    dayahead=pd.concat([DateStamp_ren, hour_ren, weekdays_ren, dayahead1],axis=1).reset_index(level=['SP'])

    data_ren['settlementPeriod'] = data_ren['settlementPeriod'].values.astype("int")
    data_ren.sort_values(by=['settlementPeriod'], ascending=[True], inplace=True)

    Solar = data_ren.loc[data_ren['powerSystemResourceType'] == '"Solar"'][['settlementPeriod', 'quantity']].rename(index=str, columns={"settlementPeriod": "SP", "quantity": "Solar"}).set_index(["SP"])
    Onshore = data_ren.loc[data_ren['powerSystemResourceType'] == '"Wind Onshore"'][['settlementPeriod', 'quantity']].rename(index=str, columns={"settlementPeriod": "SP","quantity": "WindOnshore"}).set_index(["SP"])
    Offshore = data_ren.loc[data_ren['powerSystemResourceType'] == '"Wind Offshore"'][['settlementPeriod', 'quantity']].rename(index=str, columns={"settlementPeriod": "SP","quantity": "WindOffshore"}).set_index(["SP"])
    
    Renewables=pd.concat([Solar,Onshore,Offshore],axis=1)
    Wind=pd.DataFrame(Renewables["WindOnshore"].values.astype("float32")+Renewables["WindOffshore"].values.astype("float32"), columns=["Wind"])
    Renewables.reset_index(drop=True, inplace=True)
    dayahead.reset_index(drop=True,inplace=True)
    Output_data=pd.concat([dayahead,Renewables,Wind],axis=1)

    Output_data[['SP', 'Weekday']] = Output_data[['SP', 'Weekday']].astype(int)
    Output_data[['NDF', 'TSDF', 'Solar', 'WindOnshore', 'WindOffshore']] = Output_data[['NDF', 'TSDF', 'Solar',
                                                                                        'WindOnshore','WindOffshore']].astype(float)
    return Output_data

def Period_ELEXON(start,end):
    #Pulls data from Elexon api for a given date range, can use same start and end date to grab one day

    index=pd.date_range(start,end)
    
    #The tags which are going to be added to the url that will call the data
    #are defined here in dictionaries. The data required is contained in two
    #reports: B1440 and FORDAYDEM
    
    renewables_key={'report':'B1440','SettlementDate':start,'Period':'*','Data col name':'WindAndSolar'}
    
    input_D1=dict(renewables_key)
    del input_D1['Data col name']
    print(input_D1) 

    #collecting data using function that grabs XML
    data_ren = xml2df(BMRS_GetXML(**input_D1))

    #loop through and add all extra days in
    if start != end:
        cols = data_ren.columns
        data_ren=[]
        for date in index:

            renewables_key={'report':'B1440','SettlementDate':date,'Period':'*','Data col name':'WindAndSolar'}

            input_D1=dict(renewables_key)
            del input_D1['Data col name']
            print(input_D1)

            #collecting data using function that grabs XML
            data_ren_oneiter = xml2df(BMRS_GetXML(**input_D1)).values.tolist()
            data_ren.extend(data_ren_oneiter)

        data_ren=pd.DataFrame(data_ren, columns=cols)

    
    dayahead_key={'report':'FORDAYDEM','FromDate':start,'ToDate':start,'Data col name':'DayAheadDemand'}
    
    input_D2=dict(dayahead_key)
    del input_D2['Data col name']
    print(input_D2)  

    #collecting data using function that grabs XML
    data_day = xml2df(BMRS_GetXML(**input_D2))

    if start != end:
        cols = data_day.columns
        data_day=[]
        for date in index:

            dayahead_key={'report':'FORDAYDEM','FromDate':date,'ToDate':date,'Data col name':'DayAheadDemand'}

            input_D2=dict(dayahead_key)
            del input_D2['Data col name']
            print(input_D2)

            #collecting data using function that grabs XML
            data_day_oneiter = xml2df(BMRS_GetXML(**input_D2)).values.tolist()
            data_day.extend(data_day_oneiter)

        data_day=pd.DataFrame(data_day, columns=cols)

    # processing renewables data (solar, wind)
    data_ren = data_ren.loc[data_ren['processType'].str.lower() == 'day ahead']

    data_ren['settlementPeriod'] = data_ren['settlementPeriod'].values.astype("int")
    data_ren['quantity'] = data_ren['quantity'].values.astype("float32")
    data_ren.sort_values(by=['settlementDate','settlementPeriod'], ascending=[True,True])

    Solar=data_ren.loc[data_ren['powerSystemResourceType']=='"Solar"'][['settlementDate','settlementPeriod','quantity']].\
        rename(index=str, columns={"settlementPeriod":"SP","quantity":"Solar"})#.set_index(["SP"])
    Onshore=data_ren.loc[data_ren['powerSystemResourceType']=='"Wind Onshore"'][['settlementDate','settlementPeriod','quantity']].\
        rename(index=str, columns={"settlementPeriod":"SP","quantity":"WindOnshore"})#.set_index(["SP"])
    Offshore=data_ren.loc[data_ren['powerSystemResourceType']=='"Wind Offshore"'][['settlementDate','settlementPeriod','quantity']].\
        rename(index=str, columns={"settlementPeriod":"SP","quantity":"WindOffshore"})#.set_index(["SP"])

    Renewables = pd.merge(Solar, Onshore, how='left', left_on=['SP', 'settlementDate'], right_on=['SP', 'settlementDate'])
    Renewables = pd.merge(Renewables, Offshore, how='left', left_on=['SP', 'settlementDate'],right_on=['SP', 'settlementDate'])
    Renewables['Wind'] = Renewables['WindOnshore'] + Renewables['WindOffshore']

    # Processing demand forecast (NDF and TSDF)
    NDF=data_day[data_day.recordType=='DANF'][['settlementDate','settlementPeriod','demand']].rename(index=str, columns={"settlementDate":"SD","settlementPeriod": "SP","demand":"NDF"})
    TSDF=data_day[data_day.recordType=='DATF'][['settlementDate','settlementPeriod','demand']].rename(index=str, columns={"settlementDate":"SD","settlementPeriod":"SP","demand":"TSDF"})

    days_list_ren=NDF["SD"].values.tolist()
    weekdays_ren, DateStamp_ren, hour_ren = get_weekdays(days_list_ren)

    dayahead1 = pd.merge(NDF,TSDF,how='left',on=['SD','SP'])
    dayahead=pd.concat([DateStamp_ren, hour_ren, weekdays_ren, dayahead1],axis=1)

    dayahead['SP'] = dayahead['SP'].values.astype("int")

    # joining renewables and day ahead data together, based on SP and Settlement Date
    Output_data = pd.merge(Renewables,dayahead,how='outer', left_on=['SP', 'settlementDate'],
                          right_on=['SP', 'DateStamp'])


    Output_data[['Weekday']] = Output_data[['Weekday']].astype(int)
    Output_data[['NDF', 'TSDF', 'Solar', 'WindOnshore', 'WindOffshore','Wind']] = Output_data[['NDF', 'TSDF', 'Solar', 'WindOnshore',
                                                                                'WindOffshore','Wind']].astype(float)
    #print(Output_data.dtypes)

    return Output_data
